import DashboardLayout from '../pages/layouts/DashboardLayout.vue'
import NotFound from '../pages/NotFoundPage.vue'
import Overview from '../pages/Overview.vue'
import Login from '../pages/authPages/Login.vue';
import rolesRoutes from "../routes/AccessManagement/rolesRoutes";
import permissionsRoutes from "../routes/AccessManagement/permissionsRoutes";
import usersRoutes from "../routes/AccessManagement/usersRoutes";
import UpdatePassword from "../pages/profile/UpdatePassword";
import UpdateProfile from "../pages/profile/UpdateProfile";
import ForgetPassword from "../pages/authPages/ForgetPassword";
import ResetPassword from "../pages/authPages/ResetPassword";
import customerRoutes from "./customer/customerRoutes";

let dashboardRoutes = {
    path: '/',
    component: DashboardLayout,
    redirect: '/overview',
    children: [
        {
            path: 'overview',
            name: 'Overview',
            component: Overview,
        },
        {
            path: 'change-my-password',
            name: 'Update Password',
            component: UpdatePassword,
        },
        {
            path: 'update-profile',
            name: 'Update Profile',
            component: UpdateProfile,
        },

    ],

};

let loginPage = {
    path: '/login',
    name: 'Login',
    component: Login
};
let forgetPasswordPage = {
    path: '/forget-password',
    name: 'Forget Password',
    component: ForgetPassword
};

let resetPassword = {
    path: '/reset-password/:token',
    name: 'Reset Password',
    component: ResetPassword
};

const routes = [
    {
        path: '/',
        redirect: '/overview'
    },
    loginPage,
    forgetPasswordPage,
    resetPassword,
    rolesRoutes,
    permissionsRoutes,
    usersRoutes,
    dashboardRoutes,
    customerRoutes,
    {path: '*', name: 'Not Found', component: NotFound}
]

export default routes
