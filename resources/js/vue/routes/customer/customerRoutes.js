import DashboardLayout from "../../pages/layouts/DashboardLayout";

let customerRoutes = {
  path: '/customer',
  component: DashboardLayout,
  redirect: '/customer/list',
  children: [
    {
      path: 'list',
      name: 'customer List',
      component: () => import(/* webpackChunkName: "team" */  '../../pages/customer/CustomerList'),
      meta: {
        permissionsCodes: ['customer/index' ],
      }
    },
    {
      path: 'create',
      name: 'Add customer',
      component: () => import(/* webpackChunkName: "team" */  '../../pages/customer/CustomerForm'),
      meta: {
        permissionsCodes: ['customer/create'],
      }
    },
    {
      path: 'edit/:id',
      name: 'Edit customer',
      component: () => import(/* webpackChunkName: "team" */  '../../pages/customer/CustomerForm'),
      meta: {
        permissionsCodes: ['customer/update' ],
      }
    }
  ]
}

export default customerRoutes;
