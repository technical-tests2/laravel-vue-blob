<?php


use App\Http\Controllers\Cms\AccessManagement\AuthController;
use App\Http\Controllers\Cms\AccessManagement\PermissionController;
use App\Http\Controllers\Cms\AccessManagement\RoleController;
use App\Http\Controllers\Cms\AccessManagement\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('forget-password', [AuthController::class, 'forgotPassword']);
    Route::post('reset-password/{code}', [AuthController::class, 'resetPassword']);
});

Route::group(['middleware' => ['auth:cms'], 'prefix' => 'auth'], function ($router) {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('change-my-password', [AuthController::class, 'changeMyPassword']);
    Route::post('update-profile', [AuthController::class, 'updateProfile']);
});

Route::group(['middleware' => ['auth:cms', 'permissionCheck'], 'prefix' => 'roles'], function ($router) {
    Route::post('builder', [RoleController::class, 'builder']);
    Route::post('index', [RoleController::class, 'index']);
    Route::get('get/{id}', [RoleController::class, 'get']);
    Route::post('create', [RoleController::class, 'create']);
    Route::put('update/{id}', [RoleController::class, 'update']);
    Route::delete('delete', [RoleController::class, 'delete']);

});

Route::group(['middleware' => ['auth:cms', 'permissionCheck'], 'prefix' => 'permissions'], function ($router) {
    Route::post('index', [PermissionController::class, 'index']);
    Route::get('get/{id}', [PermissionController::class, 'get']);
    Route::post('create', [PermissionController::class, 'create']);
    Route::put('update/{id}', [PermissionController::class, 'update']);
    Route::delete('delete', [PermissionController::class, 'delete']);
});

Route::group(['middleware' => ['auth:cms', 'permissionCheck'], 'prefix' => 'users'], function ($router) {
    Route::post('builder', [UserController::class, 'builder']);
    Route::post('index', [UserController::class, 'index']);
    Route::get('get/{id}', [UserController::class, 'get']);
    Route::post('create', [UserController::class, 'create']);
    Route::put('update/{id}', [UserController::class, 'update']);
    Route::delete('delete', [UserController::class, 'delete']);
    Route::post('change-password/{id}', [UserController::class, 'changePassword']);
    Route::post('change-active-status/{id}', [UserController::class, 'changeActiveStatus']);
});
