<?php


use App\Http\Controllers\Cms\Customer\CustomerController;
use Illuminate\Support\Facades\Route;


//global routes no need for permissions here


Route::group(['middleware' => ['auth:cms'], 'prefix' => 'customer'], function ($router) {

    Route::post('create', [CustomerController::class, 'create']);
    Route::post('index', [CustomerController::class, 'index']);
    Route::post('get', [CustomerController::class, 'get']);
    Route::post('update/{id}', [CustomerController::class, 'update']);
    Route::delete('delete', [CustomerController::class, 'delete']);
    Route::post('change-active-status/{id}', [CustomerController::class, 'changeActiveStatus']);
});






