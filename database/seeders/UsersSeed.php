<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
                [
                    'name' => 'super admin',
                ],[
                    'name' => 'permissions test',
                ],
            ]
        );
        DB::table('users')->insert([
            [
                'first_name' =>'Admin',
                'last_name' =>'Admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
                'is_active' => 1,
                'role_id' => 1,
            ],
            [
                'first_name' =>'permissions',
                'last_name' =>'Test',
                'email' => 'permissions@test.com',
                'password' => Hash::make('test'),
                'is_active' => 1,
                'role_id' => 2,
            ],
        ]
        );
    }
}
