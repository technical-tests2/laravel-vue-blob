<?php
namespace Database\Seeders;

use App\Models\AccessManagement\Permissions;
use App\Models\AccessManagement\PermissionGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeed extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*************************Access Management Permissions*************************/
        $accessManagement = PermissionGroup::create(['name'=> 'Access Management']);
        $customer = PermissionGroup::create(['name'=> 'customer']);


        //Role Permissions
        $accessManagement->permissions()->create(['title' => 'Role List'])->slugs()->createMany([
            ['permission_slug' => 'roles/index']
        ]);
        $accessManagement->permissions()->create(['title' => 'Create Role'])->slugs()->createMany([
            ['permission_slug' => 'roles/create'],
            ['permission_slug' => 'roles/builder']
        ]);
        $accessManagement->permissions()->create(['title' => 'Update Role'])->slugs()->createMany([
            ['permission_slug' => 'roles/update'],
            ['permission_slug' => 'roles/get'],
            ['permission_slug' => 'roles/builder']
        ]);
        $accessManagement->permissions()->create(['title' => 'Delete Role'])->slugs()->createMany([
            ['permission_slug' => 'roles/delete']
        ]);
        //Users Permissions
        $accessManagement->permissions()->create(['title' => 'User List'])->slugs()->createMany([
            ['permission_slug' => 'users/index']
        ]);
        $accessManagement->permissions()->create(['title' => 'Create User'])->slugs()->createMany([
            ['permission_slug' => 'users/create'],
            ['permission_slug' => 'users/builder']
        ]);
        $accessManagement->permissions()->create(['title' => 'Update User'])->slugs()->createMany([
            ['permission_slug' => 'users/update'],
            ['permission_slug' => 'users/get'],
            ['permission_slug' => 'users/builder']
        ]);
        $accessManagement->permissions()->create(['title' => 'Delete User'])->slugs()->createMany([
            ['permission_slug' => 'users/delete']
        ]);
        $accessManagement->permissions()->create(['title' => 'Change User Password'])->slugs()->createMany([
            ['permission_slug' => 'users/change-password']
        ]);
        $accessManagement->permissions()->create(['title' => 'Change User Status'])->slugs()->createMany([
            ['permission_slug' => 'users/change-active-status']
        ]);

        /*************************customer Permissions*************************/

        $customer->permissions()->create(['title' => 'Customer List'])->slugs()->createMany([
            ['permission_slug' => 'customer/index'],
        ]);
        $customer->permissions()->create(['title' => 'Create Customer'])->slugs()->createMany([
            ['permission_slug' => 'customer/create'],
        ]);
        $customer->permissions()->create(['title' => 'Update Customer'])->slugs()->createMany([
            ['permission_slug' => 'customer/update'],
            ['permission_slug' => 'customer/get'],
        ]);
        $customer->permissions()->create(['title' => 'Delete Customer'])->slugs()->createMany([
            ['permission_slug' => 'customer/delete'],
        ]);
        $customer->permissions()->create(['title' => 'Change Customer Status'])->slugs()->createMany([
            ['permission_slug' => 'customer/change-active-status'],
        ]);

    }
}
