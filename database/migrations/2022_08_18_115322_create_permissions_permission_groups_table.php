<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsPermissionGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions_permission_groups', function (Blueprint $table) {
            $table->id();

            $table->foreignId('permissions_id')->constrained('permissions')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();


            $table->foreignId('group_id')->constrained('permission_groups')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions_permission_groups');
    }
}
