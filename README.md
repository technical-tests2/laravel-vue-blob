
## Run Laravel

- execute : composer install 
- execute : npm install 
- make your database
- copy .env.example and rename it to .env 
- execute : php artisan key:generate
- execute : php artisan jwt:secret
- execute : php artisan storage:link  
- execute : php artisan migrate 
- execute : php artisan db:seed 
- check username and pwd in seeder  
- enjoy 

