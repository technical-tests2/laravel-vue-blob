<?php

namespace App\Models\AccessManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Permissions extends Model
{
    use HasFactory;

    protected $fillable = ['title'];

    public function slugs()
    {
        return $this->hasMany(PermissionSlugs::class, 'permission_id', 'id');
    }

    public function groups()
    {
        return $this->belongsToMany(PermissionGroup::class, 'permissions_permission_groups', 'permissions_id', 'group_id');
    }


    public function transformList(Collection $permissions): array
    {
        $result = array();
        foreach ($permissions as $p) {
            array_push($result, $p->transformItem());
        }
        return $result;
    }

    public function transformItem(): array
    {
        $permissionGroupModal = new PermissionGroup();
        return array(
            'id' => $this->id,
            'title' => $this->title,
            'slugs' => $this->slugs->map(function ($slug) {
                return array("permission_slug" => $slug->permission_slug);
            }),
            'groups' => $permissionGroupModal->transformList($this->groups, ['id', 'name']),
        );
    }


}
