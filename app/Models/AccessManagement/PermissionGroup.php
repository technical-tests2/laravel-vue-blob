<?php

namespace App\Models\AccessManagement;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    use ModelTrait;
    protected $table = "permission_groups";

    protected $fillable = [
        'name'
    ];


    protected $minimumAllowedKey = ['id','name', 'permissions'];
    protected $notAllowedKey = [];
    protected $specialFields;
    protected $extraFields;

    public function __construct(array $attributes = array())
    {
        $this->specialFields = [
            'created_at' => function ($value) {
                return date('Y-m-d h:i:s a', strtotime($this->created_at));
            },
            'updated_at' => function ($value) {
                return date('Y-m-d h:i:s a', strtotime($this->updated_at));
            },
        ];
        $this->extraFields = [
            'permissions' => function () {
                $permissionsModel = new Permissions();
                return $permissionsModel->transformList($this->permissions, ['id', 'title']);
            },
            ];
        parent::__construct($attributes);

    }

    public function permissions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Permissions::class, 'permissions_permission_groups', 'group_id');
    }


}
