<?php

namespace App\Models\AccessManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionSlugs extends Model
{
    use HasFactory;

    protected $fillable = ['permission_id','permission_slug'];
}
