<?php

namespace App\Models\AccessManagement;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'profile_image', 'is_active', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo(Roles::class, 'role_id', 'id');
    }


    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function checkPermission($code): bool
    {
        if (strtolower($this->role->name) == 'super admin') {
            return true;
        }
        $userPermissions = $userPermissions = $this->role->permissions->map(function ($p) {
            return $p->slugs->map(function ($s) {
                return $s->permission_slug;
            });
        })->collapse()->toArray();
        return in_array($code, $userPermissions);
    }


    public function transformList(Collection $users): array
    {
        $result = array();
        foreach ($users as $u) {
            array_push($result, $u->transformItem());
        }
        return $result;
    }

    public function transformItem(): array
    {

        return array(
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->first_name . ' ' . $this->last_name,
            'email' => $this->email,
            'mobile' => $this->mobile,
            'profile_image' => $this->profile_image,
            'is_active' => (boolean)$this->is_active,
            'role_id' => $this->role->id,
            'role' => $this->role->transformItem(),
            'permissions' => $this->role->permissions->map(function ($p) {
                return $p->slugs->map(function ($s) {
                    return $s->permission_slug;
                });
            })->collapse()
        );
    }
}
