<?php

namespace App\Models\AccessManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Roles extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function permissions()
    {
        return $this->belongsToMany(Permissions::class, 'role_permissions', 'role_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }

    public function transformList(Collection $roles): array
    {
        $result = array();
        foreach ($roles as $r) {
            array_push($result, $r->transformItem());
        }
        return $result;
    }

    public function transformItem(): array
    {
        $permissionModel = new Permissions();

        return array(
            'id' => $this->id,
            'name' => $this->name,
            'permissions' => $permissionModel->transformList($this->permissions)
        );
    }

}
