<?php

namespace App\Models\Customer;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Customer extends Model
{
    use ModelTrait;
    use HasFactory;

    protected $fillable = [
        'fname', 'lname', 'company', 'telephone', 'address', 'image', 'status', 'added_datetime'
    ];

    protected $minimumAllowedKey = ['id', 'full_name', 'telephone', 'company', 'status', 'added_datetime'];
    protected $notAllowedKey = [];
    protected $specialFields;
    protected $extraFields;

    public function __construct(array $attributes = array())
    {
        $this->specialFields = [
            'full_name' => function ($value) {
                return $this->fname . ' ' . ' ' . $this->lname;
            },
            'status' => function ($value) {
                return  (boolean)$this->status ;
            },
            'created_at' => function ($value) {
                return date('Y-m-d h:i:s a', strtotime($this->created_at));
            },
            'updated_at' => function ($value) {
                return date('Y-m-d h:i:s a', strtotime($this->updated_at));
            },
        ];
        $this->extraFields = [];
        parent::__construct($attributes);

    }


    public function setStatusAttribute($status)
    {
        $this->attributes['status'] = $status? 1:0;
    }
}
