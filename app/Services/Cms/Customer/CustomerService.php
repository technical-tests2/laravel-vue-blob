<?php

namespace App\Services\Cms\Customer;

use App\Models\Customer\Customer;
use App\Services\Cms\CrudService;
use App\Services\Cms\ResultService;

class CustomerService extends CrudService
{


    function __construct()
    {
        Parent::__construct(
            Customer::class,
            ['fname', 'lname', 'company' , 'telephone', 'address'],
            ['fname', 'lname', 'company' , 'telephone', 'address'],
            ['added_datetime','fname','lname'],
        );
    }


    public function changeActiveStatus($request, $id): ResultService
    {
        $valid = false;
        $code = 404;
        $message = 'not found';
        $query = $this->mainModel::where('id', '=', $id);
        $item = $query->get()->first();
        if ($item) {
            $item->update(['status' => $request['status']]);
            $valid = true;
            $code = 200;
            $message = 'Status updated successfully';
            return new ResultService(
                $valid,
                $code,
                $message,
                $item
            );
        }
        return new ResultService(
            $valid,
            $code,
            $message,
            $item
        );

    }
}
