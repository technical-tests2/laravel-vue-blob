<?php


namespace App\Services\Cms;




class ResultService
{
    public $valid;
    public $code;
    public $message;
    public $item;

    function __construct($valid, $code, $message, $item = null){
        $this->message = $message;
        $this->valid = $valid;
        $this->code = $code;
        $this->item = $item;
    }

}
