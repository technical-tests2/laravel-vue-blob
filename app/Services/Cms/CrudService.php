<?php


namespace App\Services\Cms;


use App\Exports\Report;
use App\Helpers\FilterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class CrudService
{

    protected $mainModel;
    protected $normalSearchColumns;
    protected $allowedAdvanceSearchColumns;
    protected $orderBy;
    protected $reportHeadings;
    protected $reportFields;
    protected $deleteOption;
    protected $sortStrategy;

    function __construct($mainModel, $normalSearchColumns = [], $allowedAdvanceSearchColumns = [], $orderBy = [], $report = [], $deleteOption = [], $sortStrategy = 'desc')
    {
        $this->mainModel = $mainModel;
        $this->normalSearchColumns = $normalSearchColumns;
        $this->allowedAdvanceSearchColumns = $allowedAdvanceSearchColumns;
        $this->orderBy = $orderBy;
        if (!empty($report)) {
            $this->reportHeadings = $report['headings'];
            $this->reportFields = $report['fields'];
        }
        $this->deleteOption = $deleteOption;
        $this->sortStrategy = $sortStrategy;
    }


    public function getData(Request $request, $perPage = 10): ResultService
    {

        $query = $this->mainModel::query();

        $query = FilterHelper::instance()->takeCareOfSearch($request, $this->normalSearchColumns, $this->allowedAdvanceSearchColumns, $query);

        if (isset($request['filters']) && $request['filters'] != null) {
            foreach ($request['filters'] as $key => $filter) {
                if ($filter && in_array($key, $this->allowedAdvanceSearchColumns)) {
                    $query = $query->where($key, $filter);
                }
            }
        }

        if (!empty($this->orderBy)) {
            foreach ($this->orderBy as $item) {
                $query->orderBy($item, $this->sortStrategy);
            }
        }
        $items = $query->paginate($perPage);

        return new ResultService(
            true,
            200,
            'success data',
            [
                'pageResponse' => $items,
                'items' => app($this->mainModel)->transformList($items)
            ]
        );
    }

    public function getItem($request): ResultService
    {
        $valid = false;
        $code = 404;
        $message = 'not found';

        $query = $this->mainModel::where('id', '=', $request['id']);
        $item = $query->get()->first();
        Log::debug(json_encode($item));
        if ($item) {
            $valid = true;
            $code = 200;
            $message = 'success data';
            $item = $item->transformItemExclude();
        }

        return new ResultService(
            $valid,
            $code,
            $message,
            $item

        );
    }

    public function create($data): ResultService
    {

        $data['creator_id'] = auth()->guard('cms')->user()->id;
        $item = $this->mainModel::create($data);
        return new ResultService(
            true,
            200,
            'Item created successfully',
            $item->transformItemInclude()
        );

    }

    public function update($data, $id): ResultService
    {
        $valid = false;
        $code = 404;
        $message = 'not found';

        $query = $this->mainModel::where('id', '=', $id);
        $item = $query->get()->first();
        if ($item) {
            $item->update($data);
            $valid = true;
            $code = 200;
            $message = 'Item Updated successfully';
            $item = $item->transformItemInclude();
        }

        return new ResultService(
            $valid,
            $code,
            $message,
            $item
        );
    }

    public function delete($request): ResultService
    {
        $valid = false;
        $code = 404;
        $message = 'not found';
        $query = $this->mainModel::where('id', '=', $request['id']);
        $item = $query->get()->first();
        if ($item) {
            if (!empty($this->deleteOption)) {
                if (count($item[$this->deleteOption['child']]) > 0) {
                    $valid = false;
                    $code = 409;
                    $message = $this->deleteOption['message'];
                } else {
                    $valid = true;
                    $code = 200;
                    $message = 'Item deleted successfully';
                }
            } else {
                $valid = true;
                $code = 200;
                $message = 'Item deleted successfully';
            }
        }
        if ($valid) {
            $item->delete();
        }
        return new ResultService(
            $valid,
            $code,
            $message,
            $item->transformItemInclude()
        );
    }

    public function changeActiveStatus($request, $id): ResultService
    {
        $valid = false;
        $code = 404;
        $message = 'not found';
        $query = $this->mainModel::where('id', '=', $id);
        $item = $query->get()->first();
        if ($item) {
            $item->update(['is_active' => $request['is_active']]);
            $valid = true;
            $code = 200;
            $message = 'Status updated successfully';
            return new ResultService(
                $valid,
                $code,
                $message,
                $item
            );
        }
        return new ResultService(
            $valid,
            $code,
            $message,
            $item
        );

    }

    public function reOrder(Request $request)
    {
        return DB::transaction(function () use ($request) {
            foreach ($request['data'] as $row) {
                $query = $this->mainModel::where('id', '=', $row['id']);
                $item = $query->get()->first();
                if ($item) {
                    $item->sort_order = $row['sort_order'];
                    $item->save();
                }
            }
        });
    }

    public function export($request): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new Report($this->mainModel, $this->reportFields, $this->reportHeadings, $request['filters']), 'page.xlsx');
    }

}
