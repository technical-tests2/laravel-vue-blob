<?php

namespace App\Services\Cms\AccessManagement;


use App\Http\Requests\Cms\AccessManagement\RoleRequest;
use App\Models\AccessManagement\Permissions;
use App\Models\AccessManagement\PermissionGroup;
use App\Models\AccessManagement\Roles;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleService
{
    use ApiResponse;

    public function builder(Request $request): \Illuminate\Http\JsonResponse
    {
        $groups = PermissionGroup::all();
        $permissions = Permissions::all();
        $modelClass = new Permissions();
        $permissions = $modelClass->transformList($permissions);
        $modelGroupClass = new PermissionGroup();
        return $this->showAll([
            "permissions" => $permissions,
            "groups" => $modelGroupClass->transformList($groups),
        ]);
    }

    public function getData(Request $request): \Illuminate\Http\JsonResponse
    {

        $perPage = 10;

        if (isset($request['pageSize'])) {
            $perPage = $request['pageSize'];
        }

        $query = Roles::with('permissions');

        if (isset($request['searchQuery'])) {
            $query->where('name', 'like', '%' . $request['searchQuery'] . '%');
        }

        $roles = $query->paginate($perPage);

        $modelClass = new Roles();

        return $this->paginatedResponse($roles, $modelClass->transformList($roles->getCollection()), 200);
    }

    public function getItem($id): \Illuminate\Http\JsonResponse
    {
        $role = Roles::findOrFail($id);
        return $this->showOne($role->transformItem());
    }

    public function create(Request $request): \Illuminate\Http\JsonResponse
    {
        app()->make(RoleRequest::class)->validated();

        return DB::transaction(function () use ($request) {

            $role = Roles::create($request->only('name'));

            if ($request->has('permissions') && count($request['permissions']) > 0) {

                $role->permissions()->attach($request['permissions']);

            }

            return $this->showOne($role->transformItem(), 201);

        });
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $role = Roles::findOrFail($id);

        app()->make(RoleRequest::class)->validated();

        return DB::transaction(function () use ($request, $role) {

            $role->update($request->only('name'));

            if ($request->has('permissions')) {

                $role->permissions()->sync($request['permissions']);

            }

            return $this->showOne($role->transformItem(), 200);

        });
    }

    public function delete($id): \Illuminate\Http\JsonResponse
    {
        $role = Roles::findOrFail($id);

        if (count($role->users) > 0) {
            return $this->errorResponse("There are users related to this role you can't delete it", 409);
        }
        $role->delete();
        return $this->successMessage("Item deleted successfully", 200);
    }
}
