<?php

namespace App\Services\Cms\AccessManagement;


use App\Http\Requests\Cms\AccessManagement\PermissionRequest;
use App\Models\AccessManagement\PermissionSlugs;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionsService
{
    use ApiResponse;


    public function getData(Request $request): \Illuminate\Http\JsonResponse
    {
        $perPage = 10;

        if (isset($request['pageSize'])) {
            $perPage = $request['pageSize'];
        }

        $query = Permission::select('permissions.*')
            ->leftJoin('permissions_slugs', 'permissions.id', '=', 'permissions_slugs.permission_id');

        if (isset($request['searchQuery'])) {
            $query->where('permissions.title', 'like', '%' . $request['searchQuery'] . '%')
                ->orWhere('permissions_slugs.permission_slug', 'like', '%' . $request['searchQuery'] . '%');
        }

        $query->distinct('permissions.id');

        $permissions = $query->paginate($perPage);
        $modelClass = new Permission();
        return $this->paginatedResponse($permissions, $modelClass->transformList($permissions->getCollection()), 200);
    }

    public function getItem($id): \Illuminate\Http\JsonResponse
    {
        $permission = Permission::findOrFail($id);
        return $this->showOne($permission->transformItem());
    }

    public function create(Request $request): \Illuminate\Http\JsonResponse
    {
        app()->make(PermissionRequest::class)->validated();

        return DB::transaction(function () use ($request) {

            $permission = Permission::create($request->only('title'));

            foreach ($request['slugs'] as $slug) {
                PermissionSlugs::create(['permission_id' => $permission->id, "permission_slug" => $slug["permission_slug"]]);
            }

            return $this->showOne($permission->transformItem(), 201);
        });
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $permission = Permission::findOrFail($id);

        app()->make(PermissionRequest::class)->validated();

        return DB::transaction(function () use ($permission, $request) {

            $permission->update($request->only('title'));

            PermissionSlugs::where('permission_id', $permission->id)->delete();

            foreach ($request['slugs'] as $slug) {
                PermissionSlugs::create(['permission_id' => $permission->id, "permission_slug" => $slug["permission_slug"]]);
            }

            return $this->showOne($permission->transformItem(), 200);

        });
    }

    public function delete($id): \Illuminate\Http\JsonResponse
    {
        $row = Permission::findOrFail($id);
        $row->delete();
        return $this->successMessage("Item deleted successfully", 200);
    }

}
