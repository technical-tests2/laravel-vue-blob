<?php

namespace App\Services\Cms\AccessManagement;


use App\Http\Requests\Cms\AccessManagement\UpdateUserRequest;
use App\Http\Requests\Cms\AccessManagement\UserRequest;
use App\Models\AccessManagement\Roles;
use App\Models\AccessManagement\User;
use App\Services\Cms\CrudService;
use App\Traits\ApiResponse;
use App\Traits\FileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use App\Helpers\FilterHelper;


class UserService
{
    use ApiResponse, FileTrait;


    public function builder(Request $request): \Illuminate\Http\JsonResponse
    {
        $roles = Roles::all();
        $modelClass = new Roles();
        return $this->showAll(["roles" => $modelClass->transformList($roles)]);
    }

    public function getData(Request $request): \Illuminate\Http\JsonResponse
    {
        $perPage = 10;

        if (isset($request['pageSize'])) {
            $perPage = $request['pageSize'];
        }

        $query = User::select('users.*')
            ->join('roles', 'roles.id', '=', 'users.role_id');

        if (isset($request['searchQuery'])) {
            $query->where('first_name', 'like', '%' . $request['searchQuery'] . '%')
                ->orWhere('last_name', 'like', '%' . $request['searchQuery'] . '%')
                ->orWhere('email', 'like', '%' . $request['searchQuery'] . '%')
                ->orWhere('roles.name', 'like', '%' . $request['searchQuery'] . '%');
        }

        $users = $query->paginate($perPage);
        $modelClass = new User();
        return $this->paginatedResponse($users, $modelClass->transformList($users->getCollection()), 200);
    }


    public function getItem($id): \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail($id);
        return $this->showOne($user->transformItem());
    }

    public function create(Request $request): \Illuminate\Http\JsonResponse
    {
        app()->make(UserRequest::class)->validated();

        if ($request['profile_image'] != "") {
            $profile_image = $this->movePublicFileIfInTemp($request['profile_image'], "users/profile-images");
            $request['profile_image'] = $profile_image;
        }
        $data = $request->all();
        $user = User::create($data);
        return $this->showOne($user, 201);
    }

    public function update(Request $request, $id)//: \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail($id);

        app()->make(UpdateUserRequest::class)->validated();


        if (($user->profile_image != "" &&
            ($user->profile_image != $request['profile_image']))) {
            if (\File::exists($user->profile_image)) {
                \File::delete($user->profile_image);
            }
        }

        if ($request['profile_image'] != "") {
            $profile_image = $this->movePublicFileIfInTemp($request['profile_image'], "users/profile-images");
            $request['profile_image'] = $profile_image;
        }
        $user->update($request->all());
        return $this->showOne($user, 200);

    }

    public function delete($id): \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail($id);

        if ($user->profile_image != "") {
            if (\File::exists($user->profile_image)) {
                \File::delete($user->profile_image);
            }
        }
        $user->delete();
        return $this->successMessage("User deleted successfully", 200);
    }

    public function changePassword(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'password' => ['required',
                Password::min(8)
                    // Require at least one letter...
                    ->letters()
                    // Require at least one uppercase and one lowercase letter...
                    ->mixedCase()
                    // Require at least one number...
                    ->numbers()
            ],
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $user->update(['password' => $request['password']]);

        return $this->successMessage("Password updated successfully", 200);


    }

    public function changeActiveStatus(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'is_active' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $user->update(['is_active' => $request['is_active']]);

        return $this->successMessage("Status updated successfully", 200);
    }

    public function restoreUser(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $user = User::withTrashed()->findOrFail($id);
        if($user){
            $user->restore();
        }
        return $this->successMessage("User Restored updated successfully", 200);
    }
}
