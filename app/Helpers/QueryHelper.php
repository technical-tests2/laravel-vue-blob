<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class QueryHelper
{
    public static function instance(): QueryHelper
    {
        return new QueryHelper();
    }

    public function addLang($data, $query)
    {
        if (isset($data['lang']) && $data['lang'] != "") {
            return $query->where('lang', '=', $data['lang']);
        }
        return $query;
    }

    public function addRestrictedOwn($query)
    {
        $restrictedOwn = app('shared')->get('restricted_own');
        if (isset($restrictedOwn) && $restrictedOwn) {
            return $query->where('creator_id', '=', Auth::guard('cms')->user()->id);
        }
        return $query;
    }

    public function addPublishedConditions($query , $withIsActive = true)
    {
        if($withIsActive){
            $query = $query->where('is_active',1);
        }
        return $query->where(
            function ($q) {
                $q->where(function ($query) {
                    $query->where('start_date', '<', date("Y-m-d H:i:s"))
                        ->where('end_date', '>', date("Y-m-d H:i:s"));
                     })
                    ->orWhere(function ($query) {
                        $query->where('start_date', null)
                            ->where('end_date', '>', date("Y-m-d H:i:s"));
                    })
                    ->orWhere(function ($query) {
                        $query->where('end_date', null)
                            ->where('start_date', '<', date("Y-m-d H:i:s"));
                    })
                    ->orWhere(function ($query) {
                        $query->where('end_date', null)
                            ->where('start_date', null);
                    });
            });
    }

}
