<?php

namespace App\Helpers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class Helper
{
    public static function instance(): Helper
    {
        return new Helper();
    }

    public function getNullIfNotSet($arrayData, $key)
    {
        return isset($arrayData[$key]) ? $arrayData[$key] : null;
    }

    public function getPaginated(LengthAwarePaginator $lengthAwarePaginator, $collection): array
    {
        return [
            'data' => $collection,
            'pagination' => [
                'total' => $lengthAwarePaginator->total(),
                'count' => $lengthAwarePaginator->count(),
                'per_page' => $lengthAwarePaginator->perPage(),
                'current_page' => $lengthAwarePaginator->currentPage(),
                'total_pages' => $lengthAwarePaginator->lastPage(),
            ],
        ];
    }

    function findObjectsInArrayById($id,$array): array
    {
        $result = array();

        foreach ( $array as $element ) {
            if ( $id == $element->id ) {
                $result[] = $element;
            }
        }

        return $result;
    }

    
    

}
