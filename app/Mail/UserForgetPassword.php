<?php

namespace App\Mail;

use App\Models\AccessManagement\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class UserForgetPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $code;


    public function __construct(User $user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }


    public function build()
    {

        $from = Config::get('systemConfig.fromSenderEmail');
        $appName = Config::get('systemConfig.appName');
        $subject = 'Password Reset Confirmation for ' . $this->user->first_name . ' ' . $this->user->last_name;
        $reset_password_url = Config::get('systemConfig.cmsUrl') . '/reset-password/';

        return $this->view('emails.userforgetpassword')
            ->from($from, $appName)
            ->subject($subject)
            ->with('user', $this->user)
            ->with('code', $this->code)
            ->with('reset_password_url', $reset_password_url);
    }
}
