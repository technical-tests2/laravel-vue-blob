<?php

namespace App\Exports;

use App\Helpers\FilterHelper;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Report implements FromCollection, WithHeadings
{
    private $filters;
    protected $mainModel;
    protected $fields;
    protected $headings;

    public function __construct($model, $fields, $headings, $filters = null)
    {
        $this->filters = $filters;
        $this->mainModel = $model;
        $this->fields = $fields;
        $this->headings = $headings;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $query = $this->mainModel::select($this->fields);

        foreach ($this->filters as $filter) {

            if (isset($filter['value']) && !is_null($filter['value']) && $filter['value'] != '') {
                $query = FilterHelper::instance()->getFilter($filter, $query);
            }

        }
        return $query->get();
    }

    public function headings(): array
    {
        return [
            $this->headings,
        ];
    }
}
