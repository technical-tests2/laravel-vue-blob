<?php

namespace App\Http\Requests\Cms\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'fname' => 'required',
            'lname' => '',
            'company' => '',
            'telephone' => ['required', Rule::unique('customers','telephone')->ignore($this->id, 'id')],
            'address' => 'required',
            'image_file' => '',
            'status' => '',
        ];
    }
}
