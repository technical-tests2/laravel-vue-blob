<?php

namespace App\Http\Requests\Cms\AccessManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required', Rule::unique('users','email')->ignore($this->id, 'id')],
            'role_id' => ['required'],
            'is_active' => ['required'],
            'password' => ['required',
                Password::min(8)
                // Require at least one letter...
                ->letters()
                // Require at least one uppercase and one lowercase letter...
                ->mixedCase()
                // Require at least one number...
                ->numbers()
            ],
        ];
    }
}
