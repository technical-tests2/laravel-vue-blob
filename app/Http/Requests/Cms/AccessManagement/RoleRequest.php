<?php

namespace App\Http\Requests\Cms\AccessManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required', Rule::unique('roles','name')->ignore($this->id, 'id'),
            ],
        ];
    }
}
