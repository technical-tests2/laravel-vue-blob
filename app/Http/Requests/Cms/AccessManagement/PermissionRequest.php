<?php

namespace App\Http\Requests\Cms\AccessManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PermissionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => [
                'required', Rule::unique('permissions','title')->ignore($this->id, 'id'),
            ],
            'slugs' => 'required',
        ];
    }
}
