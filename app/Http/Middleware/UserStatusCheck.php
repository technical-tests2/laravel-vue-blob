<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserStatusCheck
{

    public function handle($request, Closure $next)
    {
        if (!Auth::guard('cms')->check()) {
            return $next($request);
        }

        $logout = false;

        if (Auth::guard('cms')->check()) {
            $activeStatus = Auth::guard('cms')->user()->is_active;
            if ($activeStatus == 0) {
                Auth::guard('cms')->logout();
                return response()->json(['message' => 'Unauthenticated.'], 401);
            }
        }

        if ($logout) {
            Auth::guard('cms')->logout();
            return response()->json(['message' => 'Unauthenticated.'], 401);
        } else {
            return $next($request);
        }
    }

}
