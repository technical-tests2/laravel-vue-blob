<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        # check user login

        if (!Auth::guard('cms')->check()) {
            return $next($request);
        }

        if (strtolower(Auth::guard('cms')->user()->role->name) == 'super admin') {
            return $next($request);
        }

        $currentPath = $request->path();
        $path = str_ireplace('api/', '', $currentPath);
        $path2 = preg_replace('/[0-9]+/', '', $path);
        $perm = rtrim($path2, '/');

        if (Auth::guard('cms')->user()->checkPermission($perm)) {
            return $next($request);
        }
        return response()->json(['message' => 'Permission denied.'], 403);
    }
}
