<?php

namespace App\Http\Controllers\Cms;

use App\Services\Cms\CrudService;
use App\Traits\ApiResponse;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Routing\Controller as BaseController;


class CrudController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiResponse;

    protected $mainService;
    protected $createRequest;
    protected $updateRequest;


    function __construct(CrudService $mainService)
    {
        $this->mainService = $mainService;

    }

    public function builder(Request $request): JsonResponse
    {
        $result = $this->mainService->builder($request);
        return $this->showAll($result->item);
    }

    public function index(Request $request): JsonResponse
    {
        $perPage = 10;

        if (isset($request['pageSize'])) {
            $perPage = $request['pageSize'];
        }

        $result = $this->mainService->getData($request, $perPage);
        if ($result->valid){
            return $this->paginatedResponse($result->item['pageResponse'], $result->item['items'], 200);
        }else{
            return $this->errorResponse($result->message, $result->code);
        }
    }

    public function get(Request $request): JsonResponse
    {
        $result = $this->mainService->getItem($request);
        if ($result->valid){
            return $this->showOne($result->item);
        }
        return $this->errorResponse($result->message, $result->code);


    }


    public function create(Request $request): JsonResponse
    {
        $data = app()->make($this->createRequest)->validated();
        $result = $this->mainService->create($data);
        if ($result->valid) {
            return $this->showOne($result->item, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }


    public function update(Request $request,$id): JsonResponse
    {
        $data = app()->make($this->updateRequest)->validated();
        $result = $this->mainService->update($data, $id);
        if ($result->valid) {
            return $this->showOne($result->item, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }

    public function delete(Request $request): JsonResponse
    {

        $result = $this->mainService->delete($request);
        if ($result->valid) {
            return $this->successMessage($result->message, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }


    public function changeActiveStatus(Request $request, $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'is_active' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $result = $this->mainService->changeActiveStatus($request, $id);
        if ($result->valid) {
            return $this->successMessage($result->message, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }

    public function reOrder(Request $request): JsonResponse
    {
        $result = $this->mainService->reOrder($request);
        return $this->successMessage("Order updated successfully", 200);
    }

    public function export(Request $request): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return $this->mainService->export($request);
    }




}
