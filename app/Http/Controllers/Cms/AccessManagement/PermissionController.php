<?php

namespace App\Http\Controllers\Cms\AccessManagement;


use App\Services\Cms\AccessManagement\PermissionsService;
use Illuminate\Http\Request;

class PermissionController
{
    protected $mainService;

    function __construct(PermissionsService $mainService)
    {
        $this->mainService = $mainService;
    }

    public function index(Request $request)
    {
        return $this->mainService->getData($request);
    }

    public function get($id)
    {
        return $this->mainService->getItem($id);
    }


    public function create(Request $request)
    {
        return $this->mainService->create($request);
    }


    public function update(Request $request, $id)
    {
        return $this->mainService->update($request, $id);
    }

    public function delete(Request $request)
    {
        return $this->mainService->delete($request['id']);
    }

}
