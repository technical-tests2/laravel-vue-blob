<?php

namespace App\Http\Controllers\Cms\AccessManagement;

use App\Services\Cms\AccessManagement\RoleService;
use Illuminate\Http\Request;


class RoleController
{
    protected $mainService;

    function __construct(RoleService $mainService)
    {
        $this->mainService = $mainService;
    }

    public function builder(Request $request)
    {
        return $this->mainService->builder($request);
    }

    public function index(Request $request)
    {
        return $this->mainService->getData($request);
    }

    public function get($id)
    {
        return $this->mainService->getItem($id);
    }


    public function create(Request $request)
    {
        return $this->mainService->create($request);
    }


    public function update(Request $request, $id)
    {
        return $this->mainService->update($request, $id);
    }

    public function delete(Request $request)
    {
        return $this->mainService->delete($request['id']);
    }
}
