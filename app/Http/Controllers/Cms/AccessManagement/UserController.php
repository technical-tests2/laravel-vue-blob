<?php

namespace App\Http\Controllers\Cms\AccessManagement;

use App\Services\Cms\AccessManagement\UserService;
use Illuminate\Http\Request;

class UserController
{

    protected $mainService;


    function __construct(UserService $mainService)
    {
        $this->mainService = $mainService;
    }

    public function builder(Request $request)
    {
        return $this->mainService->builder($request);
    }

    public function index(Request $request)
    {
        return $this->mainService->getData($request);
    }

    public function getDeletedUsers(Request $request)
    {
        return $this->mainService->getDeletedUsers($request);
    }

    public function get($id)
    {
        return $this->mainService->getItem($id);
    }

    public function getGoogle2FaInfo(Request $request){
        return $this->mainService->getGoogle2FaInfo($request['id']);
    }

    public function create(Request $request)
    {
        return $this->mainService->create($request);
    }


    public function update(Request $request, $id)
    {
        return $this->mainService->update($request, $id);
    }

    public function delete(Request $request)
    {
        return $this->mainService->delete($request['id']);
    }

    public function changePassword(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        return $this->mainService->changePassword($request, $id);
    }

    public function changeActiveStatus(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        return $this->mainService->changeActiveStatus($request, $id);
    }

    public function restoreUser(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        return $this->mainService->restoreUser($request, $id);
    }
}
