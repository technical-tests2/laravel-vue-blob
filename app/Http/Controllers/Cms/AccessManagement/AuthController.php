<?php

namespace App\Http\Controllers\Cms\AccessManagement;

use App\Http\Controllers\Controller;
use App\Mail\UserForgetPassword;
use App\Models\AccessManagement\User;
use App\Traits\ApiResponse;
use App\Traits\FileTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    use ApiResponse,FileTrait;

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (!$token = auth()->guard('cms')->attempt($credentials)) {
            return $this->errorResponse("Unauthorized, Email or password not correct", 401);
        }

        if (auth()->guard('cms')->user()->is_active == 0) {
            return $this->errorResponse('Your Account has been deactivated', 401);
        }

        return response()->json([
            'access_token' => $token,
            'user' => auth()->guard('cms')->user()->transformItem()
        ]);

    }

    public function me(): \Illuminate\Http\JsonResponse
    {
        if (auth()->guard('cms')->user()->is_active == 0) {
            return $this->errorResponse('Your Account has been deactivated', 401);
        } else {
            return $this->showOne((auth()->guard('cms')->user()->transformItem()));
        }
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->guard('cms')->logout();

        return $this->successMessage("Successfully logged out", 200);
    }

    public function forgotPassword(Request $request): \Illuminate\Http\JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $email = $request['email'];
        $user = User::where('email', $email)->first();

        if (!$user || $user->is_active == 0) {
            return $this->errorResponse("This email ($email) doesn't exist", 400);
        }

        $code = uniqid() . Str::random(60);
        $user->forget_password_code = $code;
        $user->forget_password_date = Carbon::now();
        $user->save();

        try {
            Mail::to($email)->send(new UserForgetPassword($user, $code));
            return $this->successMessage("The email has been sent successfully", 200);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 400);
        }

    }

    public function resetPassword(Request $request, $code): \Illuminate\Http\JsonResponse
    {

        $validator = Validator::make($request->all(), ['password' => 'required']);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $user = User::where('forget_password_code', $code)->first();

        if (!$user ||
            (Carbon::parse($user->forget_password_date)->addHour(24)->toDateTimeString()
                < Carbon::now()->toDateTimeString())) {
            return $this->errorResponse('Your token is not active anymore', 400);
        }

        $user->password = $request['password'];
        $user->forget_password_code = null;
        $user->forget_password_date = null;
        $user->save();

        return $this->successMessage("Password updated successfully", 200);

    }

    public function changeMyPassword(Request $request): \Illuminate\Http\JsonResponse
    {

        $user = User::findOrFail(auth()->guard('cms')->user()->id);

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        if (Hash::check($request['old_password'], $user->password)) {
            $user->password = $request['new_password'];
            $user->save();
            return $this->successMessage("Password updated successfully", 200);
        } else {

            $validator->after(function ($validator) {
                $validator->errors()->add('old_password', "Your old password is wrong");
            });

            if ($validator->fails()) {
                return response()->json([
                    "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
                ], 422);
            }
        }
    }

    public function updateProfile(Request $request): \Illuminate\Http\JsonResponse
    {
        $user = User::findOrFail(auth()->guard('cms')->user()->id);

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'nullable|unique:users,mobile,' . $user->id,
            'email' => 'required|unique:users,email,' . $user->id
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $request->request->remove("role_id");

        if (($user->profile_image != "" &&
            ($user->profile_image != $request['profile_image']))) {
            if (\File::exists($user->profile_image)) {
                \File::delete($user->profile_image);
            }
        }

        if ($request['profile_image'] != "") {
            $profile_image = $this->movePublicFileIfInTemp($request['profile_image'], "cmsusers/profile-images");
            $request['profile_image'] = $profile_image;
        }

        $user->update($request->all());

        return $this->successMessage("Profile updated successfully", 200);
    }

}
