<?php

namespace App\Http\Controllers\Cms\Customer;


use App\Http\Controllers\Cms\CrudController;
use App\Http\Requests\Cms\Customer\CustomerRequest;
use App\Models\Customer\Customer;
use App\Services\Cms\Customer\CustomerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends CrudController
{

    protected $mainService;
    protected $createRequest;
    protected $updateRequest;

    function __construct(CustomerService $mainService)
    {
        $this->createRequest = CustomerRequest::class;
        $this->updateRequest = CustomerRequest::class;
        Parent::__construct($mainService);

    }

    public function create(Request $request): JsonResponse
    {
        $data = app()->make($this->createRequest)->validated();
        if($request->hasFile('image_file')){
            $path = $request->file('image_file')->store('front-temp');
            $image = base64_encode(file_get_contents(  "storage/" .$path));
            $data['image'] = $image;
        }

        $result = $this->mainService->create($data);
        if ($result->valid) {
            return $this->showOne($result->item, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }


    public function update(Request $request,$id): JsonResponse
    {
        $data = app()->make($this->updateRequest)->validated();
        if($request->hasFile('image_file')){
            $path = $request->file('image_file')->store('front-temp');
            $image = base64_encode(file_get_contents(  "storage/" .$path));
            $data['image'] = $image;
        }
        $result = $this->mainService->update($data, $id);
        if ($result->valid) {
            return $this->showOne($result->item, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }

    public function changeActiveStatus(Request $request, $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        $result = $this->mainService->changeActiveStatus($request, $id);
        if ($result->valid) {
            return $this->successMessage($result->message, $result->code);
        }
        return $this->errorResponse($result->message, $result->code);
    }

}
