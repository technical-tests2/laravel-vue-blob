"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["roles"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleForm.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleForm.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var element_ui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! element-ui */ "./node_modules/element-ui/lib/element-ui.common.js");
/* harmony import */ var element_ui__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(element_ui__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/index */ "./resources/js/vue/components/index.js");
/* harmony import */ var _components_Inputs_formGroupSelect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/Inputs/formGroupSelect */ "./resources/js/vue/components/Inputs/formGroupSelect.vue");
var _components;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







(0,vee_validate__WEBPACK_IMPORTED_MODULE_3__.extend)("required", {
  message: "{_field_} is required"
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: (_components = {}, _defineProperty(_components, element_ui__WEBPACK_IMPORTED_MODULE_0__.Option.name, element_ui__WEBPACK_IMPORTED_MODULE_0__.Option), _defineProperty(_components, element_ui__WEBPACK_IMPORTED_MODULE_0__.Select.name, element_ui__WEBPACK_IMPORTED_MODULE_0__.Select), _defineProperty(_components, element_ui__WEBPACK_IMPORTED_MODULE_0__.Table.name, element_ui__WEBPACK_IMPORTED_MODULE_0__.Table), _defineProperty(_components, element_ui__WEBPACK_IMPORTED_MODULE_0__.TableColumn.name, element_ui__WEBPACK_IMPORTED_MODULE_0__.TableColumn), _defineProperty(_components, "LSwitch", _components__WEBPACK_IMPORTED_MODULE_1__.Switch), _defineProperty(_components, "LPagination", _components__WEBPACK_IMPORTED_MODULE_1__.Pagination), _defineProperty(_components, "FgSelect", _components_Inputs_formGroupSelect__WEBPACK_IMPORTED_MODULE_2__["default"]), _components),
  computed: {
    queriedData: function queriedData() {
      var _this = this;

      var result = this.tableData;

      if (this.searchQuery !== '') {
        result = result.filter(function (_ref) {
          var title = _ref.title;
          return title.toLowerCase().includes(_this.searchQuery);
        });
      }

      if (this.filters.group_id !== "" && this.filters.group_id !== null) {
        result = result.filter(function (_ref2) {
          var groups = _ref2.groups;
          return groups.find(function (item) {
            return item.id === _this.filters.group_id;
          });
        });
      }

      this.pagination.total = result.length;
      return result.slice(this.from, this.to);
    },
    to: function to() {
      var highBound = this.from + this.pagination.perPage;

      if (this.total < highBound) {
        highBound = this.total;
      }

      return highBound;
    },
    from: function from() {
      return this.pagination.perPage * (this.pagination.currentPage - 1);
    },
    total: function total() {
      this.pagination.total = this.tableData.length;
      return this.tableData.length;
    }
  },
  data: function data() {
    return {
      tableData: [],
      fuseSearch: null,
      pagination: {
        perPage: 10,
        currentPage: 1,
        perPageOptions: [5, 10, 25, 50],
        total: 0
      },
      propsToSearch: ['title'],
      searchQuery: '',
      tableColumns: [{
        label: 'Title',
        value: 'title',
        minWidth: '200',
        align: 'center'
      }],
      editMode: false,
      loader: {},
      id: undefined,
      formTitle: "",
      entityNotFoundError: false,
      submitting: false,
      formData: {
        name: "",
        permissions: []
      },
      permissions: [],
      filterBuilders: {
        groups: []
      },
      filters: {
        group_id: null
      }
    };
  },
  mounted: function mounted() {
    var _this2 = this;

    var data = {
      'lang': this.lang
    };
    this.loader = this.$loading.show({
      container: this.$refs.roleForm
    });
    this.axios.post("roles/builder", data).then(function (response) {
      _this2.permissions = response.data.permissions;
      _this2.filterBuilders.groups = response.data.groups;
      _this2.id = _this2.$route.params['id'];
      _this2.tableData = _this2.permissions.map(function (item) {
        return _objectSpread(_objectSpread({}, item), {}, {
          'is_active': false
        });
      });

      _this2.loader.hide();

      if (_this2.id !== undefined) {
        _this2.editMode = true;
        _this2.formTitle = "Edit Role";

        _this2.getRole();
      } else {
        _this2.editMode = false;
        _this2.formTitle = "Add Role";

        _this2.loader.hide();
      }
    })["catch"](function (error) {
      console.error(error);
    });
  },
  methods: {
    getRole: function getRole() {
      var _this3 = this;

      this.axios.get("roles/get/" + this.id).then(function (response) {
        _this3.formData.name = response.data.name;
        _this3.formData.permissions = response.data.permissions.map(function (p) {
          return p.id;
        });

        _this3.tableData.map(function (item) {
          if (_this3.formData.permissions.includes(item.id)) {
            item.is_active = true;
          } else {
            item.is_active = false;
          }
        });

        _this3.loader.hide();
      })["catch"](function (error) {
        if (error.response.status === 404) {
          _this3.entityNotFoundError = true;

          _this3.$notify({
            message: "Role Not Found",
            timeout: 2000,
            type: 'danger'
          });

          _this3.loader.hide();
        } else {
          console.error(error);
        }
      });
    },
    submit: function submit() {
      var _this4 = this;

      var request;
      var successMessage;
      this.submitting = true;
      this.formData.permissions = this.tableData.filter(function (element) {
        return element.is_active === true;
      });
      this.formData.permissions = this.formData.permissions.map(function (p) {
        return p.id;
      });

      if (this.editMode === true) {
        request = this.axios.put("roles/update/" + this.id, this.formData);
        successMessage = "Role Updated Successfully";
      } else {
        request = this.axios.post("roles/create", this.formData);
        successMessage = "Role Added Successfully";
      }

      request.then(function (response) {
        _this4.$notify({
          message: successMessage,
          timeout: 1000,
          type: 'success'
        });

        _this4.$router.push("/roles/list");
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this4.$refs.formValidator.setErrors(error.response.data.errors);
        } else {
          console.log(error.response);
        }
      })["finally"](function () {
        _this4.submitting = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleList.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleList.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../components */ "./resources/js/vue/components/index.js");
/* harmony import */ var _components_GeneralDataTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/GeneralDataTable */ "./resources/js/vue/components/GeneralDataTable.vue");
/* harmony import */ var element_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! element-ui */ "./node_modules/element-ui/lib/element-ui.common.js");
/* harmony import */ var element_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(element_ui__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) { if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; } return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) { keys.push(key); } return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) { "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); } }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: _defineProperty({
    GeneralDataTable: _components_GeneralDataTable__WEBPACK_IMPORTED_MODULE_1__["default"],
    DeleteModal: _components__WEBPACK_IMPORTED_MODULE_0__.DeleteModal
  }, element_ui__WEBPACK_IMPORTED_MODULE_2__.TableColumn.name, element_ui__WEBPACK_IMPORTED_MODULE_2__.TableColumn),
  data: function data() {
    return {
      tableColumns: [{
        label: 'Name',
        value: 'name',
        minWidth: '200',
        align: 'center'
      }],
      deleteModalVisibility: false,
      toDeleteId: undefined
    };
  },
  methods: {
    openDeleteModal: function openDeleteModal(id) {
      this.deleteModalVisibility = true;
      this.toDeleteId = id;
    },
    closeDeleteModal: function closeDeleteModal() {
      this.deleteModalVisibility = false;
    },
    confirmDeleteModal: function confirmDeleteModal() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var data;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                data = {
                  'id': _this.toDeleteId
                };
                _context.next = 4;
                return _this.axios["delete"]("roles/delete", {
                  headers: {},
                  data: data
                });

              case 4:
                _this.$refs.table.getData("updateData");

                _this.$notify({
                  message: "Role deleted successfully",
                  timeout: 1000,
                  type: 'success'
                });

                _context.next = 11;
                break;

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);

                _this.$notify({
                  message: _context.t0.response.data.message,
                  timeout: 2000,
                  type: 'danger'
                });

              case 11:
                _context.prev = 11;

                _this.closeDeleteModal();

                _this.toDeleteId = undefined;
                return _context.finish(11);

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8, 11, 15]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleForm.vue?vue&type=template&id=3370cac4&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleForm.vue?vue&type=template&id=3370cac4& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    ref: "roleForm",
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-xs-8 offset-xs-2 col-md-10 offset-md-1 col-sm-12"
  }, [_c("ValidationObserver", {
    ref: "formValidator",
    scopedSlots: _vm._u([{
      key: "default",
      fn: function fn(_ref) {
        var handleSubmit = _ref.handleSubmit;
        return [_c("card", [_c("div", {
          attrs: {
            slot: "header"
          },
          slot: "header"
        }, [_c("h4", {
          staticClass: "card-title"
        }, [_vm._v("\n                        " + _vm._s(_vm.formTitle) + "\n                    ")])]), _vm._v(" "), _c("div", {
          staticClass: "card-body"
        }, [_c("ValidationProvider", {
          attrs: {
            vid: "name",
            rules: "required",
            name: "The name"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref2) {
              var passed = _ref2.passed,
                  failed = _ref2.failed,
                  errors = _ref2.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "text",
                  error: failed ? errors[0] : null,
                  label: "Name",
                  name: "name",
                  fou: ""
                },
                model: {
                  value: _vm.formData.name,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "name", $$v);
                  },
                  expression: "formData.name"
                }
              })];
            }
          }], null, true)
        }), _vm._v(" "), _c("div", {
          staticClass: "row table-full-width"
        }, [_c("div", {
          staticClass: "row"
        }, [_c("div", {
          staticClass: "col-md-4"
        }, [_c("el-input", {
          staticClass: "mb-3",
          staticStyle: {
            width: "200px"
          },
          attrs: {
            type: "search",
            placeholder: "Search records",
            "aria-controls": "datatables"
          },
          model: {
            value: _vm.searchQuery,
            callback: function callback($$v) {
              _vm.searchQuery = $$v;
            },
            expression: "searchQuery"
          }
        }), _vm._v(" "), _c("el-select", {
          staticClass: "select-default mb-3 mx-3",
          staticStyle: {
            width: "75px"
          },
          attrs: {
            placeholder: "Per page"
          },
          model: {
            value: _vm.pagination.perPage,
            callback: function callback($$v) {
              _vm.$set(_vm.pagination, "perPage", $$v);
            },
            expression: "pagination.perPage"
          }
        }, _vm._l(_vm.pagination.perPageOptions, function (item) {
          return _c("el-option", {
            key: item,
            staticClass: "select-default",
            attrs: {
              label: item,
              value: item
            }
          });
        }), 1)], 1), _vm._v(" "), _c("div", {
          staticClass: "col-md-8"
        }, [_c("div", {
          staticClass: "row col-12 mb-2"
        }, [_c("div", {
          staticClass: "col-md-8 col-sm-4"
        }, [_c("fg-select", {
          attrs: {
            size: "large",
            filterable: "",
            clearable: "",
            placeholder: "Select Group",
            "input-classes": "select-default",
            list: _vm.filterBuilders.groups,
            listItemLabel: "name",
            listItemValue: "id"
          },
          model: {
            value: _vm.filters.group_id,
            callback: function callback($$v) {
              _vm.$set(_vm.filters, "group_id", $$v);
            },
            expression: "filters.group_id"
          }
        })], 1)])])]), _vm._v(" "), _c("div", {
          staticClass: "col-sm-12"
        }, [_c("el-table", {
          staticClass: "table",
          attrs: {
            data: _vm.queriedData
          }
        }, [_vm._l(_vm.tableColumns, function (column) {
          return _c("el-table-column", {
            key: column.label,
            attrs: {
              "min-width": column.minWidth,
              align: column.align,
              sortable: column.sortable,
              prop: column.value,
              label: column.label
            }
          });
        }), _vm._v(" "), _c("el-table-column", {
          attrs: {
            "min-width": 120,
            fixed: "right",
            align: "center",
            label: "Active"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(props) {
              return [_c("l-switch", {
                model: {
                  value: props.row.is_active,
                  callback: function callback($$v) {
                    _vm.$set(props.row, "is_active", $$v);
                  },
                  expression: "props.row.is_active"
                }
              }, [_c("i", {
                staticClass: "fa fa-check",
                attrs: {
                  slot: "on"
                },
                slot: "on"
              }), _vm._v(" "), _c("i", {
                staticClass: "fa fa-times",
                attrs: {
                  slot: "off"
                },
                slot: "off"
              })])];
            }
          }], null, true)
        })], 2), _vm._v(" "), _c("div", {
          staticClass: "col-12 d-flex justify-content-center justify-content-sm-between flex-wrap",
          attrs: {
            slot: "footer"
          },
          slot: "footer"
        }, [_c("div", {}, [_c("p", {
          staticClass: "card-category"
        }, [_vm._v("Showing " + _vm._s(_vm.from + 1) + " to " + _vm._s(_vm.to) + " of " + _vm._s(_vm.total) + " entries")])]), _vm._v(" "), _c("l-pagination", {
          staticClass: "pagination-no-border",
          attrs: {
            "per-page": _vm.pagination.perPage,
            total: _vm.pagination.total
          },
          model: {
            value: _vm.pagination.currentPage,
            callback: function callback($$v) {
              _vm.$set(_vm.pagination, "currentPage", $$v);
            },
            expression: "pagination.currentPage"
          }
        })], 1)], 1)])], 1), _vm._v(" "), _c("div", {
          staticClass: "card-footer text-right"
        }, [_c("l-button", {
          attrs: {
            disabled: _vm.entityNotFoundError || _vm.submitting,
            nativeType: "submit",
            type: "info",
            wide: ""
          },
          on: {
            click: function click($event) {
              $event.preventDefault();
              return handleSubmit(_vm.submit);
            }
          }
        }, [_vm._v("Submit\n                    ")]), _vm._v(" "), _c("l-button", {
          attrs: {
            type: "danger",
            wide: ""
          },
          on: {
            click: function click($event) {
              return _vm.$router.push("/roles/list");
            }
          }
        }, [_vm._v("Cancel\n                    ")])], 1)])];
      }
    }])
  })], 1)]);
};

var staticRenderFns = [];
render._withStripped = true;


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleList.vue?vue&type=template&id=1a40c178&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleList.vue?vue&type=template&id=1a40c178& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-12"
  }, [_c("card", [_c("div", {
    staticClass: "col-12 d-flex justify-content-center justify-content-sm-between flex-wrap",
    attrs: {
      slot: "header"
    },
    slot: "header"
  }, [_c("h4", {
    staticClass: "card-title"
  }, [_vm._v("Role List")]), _vm._v(" "), _vm.$store.getters["auth/haveOneOfPermissions"](["roles/create"]) ? _c("router-link", {
    staticClass: "btn btn-info btn-wd",
    attrs: {
      to: "/roles/create",
      wide: ""
    }
  }, [_vm._v("\n          Add New\n          "), _c("span", {
    staticClass: "btn-label"
  }, [_c("i", {
    staticClass: "fa fa-plus"
  })])]) : _vm._e()], 1), _vm._v(" "), _c("div", [_c("general-data-table", {
    ref: "table",
    attrs: {
      "api-url": "roles/index"
    }
  }, [_vm._l(_vm.tableColumns, function (column) {
    return _c("el-table-column", {
      key: column.label,
      attrs: {
        "min-width": column.minWidth,
        align: column.align,
        prop: column.value,
        sortable: column.sortable,
        label: column.label
      }
    });
  }), _vm._v(" "), _c("el-table-column", {
    attrs: {
      "min-width": 120,
      align: "center",
      label: "Actions"
    },
    scopedSlots: _vm._u([{
      key: "default",
      fn: function fn(scope) {
        return [scope.row["name"] !== "super admin" ? [_vm.$store.getters["auth/haveOneOfPermissions"](["roles/update"]) ? _c("router-link", {
          directives: [{
            name: "tooltip",
            rawName: "v-tooltip.top-center",
            value: "Edit",
            expression: "'Edit'",
            modifiers: {
              "top-center": true
            }
          }],
          staticClass: "btn-warning btn-simple btn-link",
          attrs: {
            to: "/roles/edit/" + scope.row.id
          }
        }, [_c("i", {
          staticClass: "fa fa-edit"
        })]) : _vm._e(), _vm._v(" "), _vm.$store.getters["auth/haveOneOfPermissions"](["roles/delete"]) ? _c("a", {
          directives: [{
            name: "tooltip",
            rawName: "v-tooltip.top-center",
            value: "Delete",
            expression: "'Delete'",
            modifiers: {
              "top-center": true
            }
          }],
          staticClass: "btn-danger btn-simple btn-link",
          on: {
            click: function click($event) {
              return _vm.openDeleteModal(scope.row.id);
            }
          }
        }, [_c("i", {
          staticClass: "fa fa-times"
        })]) : _vm._e()] : _vm._e()];
      }
    }])
  })], 2)], 1)]), _vm._v(" "), _c("delete-modal", {
    attrs: {
      visible: _vm.deleteModalVisibility,
      message: "Are you sure you want to delete this role?"
    },
    on: {
      close: function close($event) {
        return _vm.closeDeleteModal();
      },
      confirm: function confirm($event) {
        return _vm.confirmDeleteModal();
      }
    }
  })], 1)]);
};

var staticRenderFns = [];
render._withStripped = true;


/***/ }),

/***/ "./resources/js/vue/pages/roles/RoleForm.vue":
/*!***************************************************!*\
  !*** ./resources/js/vue/pages/roles/RoleForm.vue ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _RoleForm_vue_vue_type_template_id_3370cac4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleForm.vue?vue&type=template&id=3370cac4& */ "./resources/js/vue/pages/roles/RoleForm.vue?vue&type=template&id=3370cac4&");
/* harmony import */ var _RoleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleForm.vue?vue&type=script&lang=js& */ "./resources/js/vue/pages/roles/RoleForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleForm_vue_vue_type_template_id_3370cac4___WEBPACK_IMPORTED_MODULE_0__.render,
  _RoleForm_vue_vue_type_template_id_3370cac4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/pages/roles/RoleForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/pages/roles/RoleList.vue":
/*!***************************************************!*\
  !*** ./resources/js/vue/pages/roles/RoleList.vue ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _RoleList_vue_vue_type_template_id_1a40c178___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleList.vue?vue&type=template&id=1a40c178& */ "./resources/js/vue/pages/roles/RoleList.vue?vue&type=template&id=1a40c178&");
/* harmony import */ var _RoleList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleList.vue?vue&type=script&lang=js& */ "./resources/js/vue/pages/roles/RoleList.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleList_vue_vue_type_template_id_1a40c178___WEBPACK_IMPORTED_MODULE_0__.render,
  _RoleList_vue_vue_type_template_id_1a40c178___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/pages/roles/RoleList.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/pages/roles/RoleForm.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/vue/pages/roles/RoleForm.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RoleForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/pages/roles/RoleList.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/vue/pages/roles/RoleList.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RoleList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleList.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/pages/roles/RoleForm.vue?vue&type=template&id=3370cac4&":
/*!**********************************************************************************!*\
  !*** ./resources/js/vue/pages/roles/RoleForm.vue?vue&type=template&id=3370cac4& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleForm_vue_vue_type_template_id_3370cac4___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleForm_vue_vue_type_template_id_3370cac4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleForm_vue_vue_type_template_id_3370cac4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RoleForm.vue?vue&type=template&id=3370cac4& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleForm.vue?vue&type=template&id=3370cac4&");


/***/ }),

/***/ "./resources/js/vue/pages/roles/RoleList.vue?vue&type=template&id=1a40c178&":
/*!**********************************************************************************!*\
  !*** ./resources/js/vue/pages/roles/RoleList.vue?vue&type=template&id=1a40c178& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleList_vue_vue_type_template_id_1a40c178___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleList_vue_vue_type_template_id_1a40c178___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleList_vue_vue_type_template_id_1a40c178___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RoleList.vue?vue&type=template&id=1a40c178& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/roles/RoleList.vue?vue&type=template&id=1a40c178&");


/***/ })

}]);