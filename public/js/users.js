"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["users"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vee-validate/dist/rules */ "./node_modules/vee-validate/dist/rules.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



(0,vee_validate__WEBPACK_IMPORTED_MODULE_0__.extend)("confirmed", vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__.confirmed);
(0,vee_validate__WEBPACK_IMPORTED_MODULE_0__.extend)("required", vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__.required);
(0,vee_validate__WEBPACK_IMPORTED_MODULE_0__.extend)("min", _objectSpread(_objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__.min), {}, {
  message: 'the password must be 8 character or more'
}));
(0,vee_validate__WEBPACK_IMPORTED_MODULE_0__.extend)("regex", _objectSpread(_objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__.regex), {}, {
  message: 'Password requires 1 of each of the following: uppercase letter, lowercase letter, number.'
}));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      formTitle: "Change Password",
      submitting: false,
      formData: {
        password: "",
        confirmed_password: ""
      },
      passVal: {
        required: true,
        min: 8,
        regex: '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).*$'
      }
    };
  },
  methods: {
    submit: function submit() {
      var _this = this;

      this.submitting = true;
      this.axios.post("users/change-password/" + this.$route.params['id'], this.formData).then(function (response) {
        _this.$notify({
          message: "Password Updated Successfully",
          timeout: 1000,
          type: 'success'
        });

        _this.$router.push("/users/list");
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this.$refs.formValidator.setErrors(error.response.data.errors);
        } else {
          console.log(error.response);
        }
      })["finally"](function () {
        _this.submitting = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserForm.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserForm.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vee-validate/dist/rules */ "./node_modules/vee-validate/dist/rules.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../components */ "./resources/js/vue/components/index.js");
/* harmony import */ var _components_Inputs_formGroupSelect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/Inputs/formGroupSelect */ "./resources/js/vue/components/Inputs/formGroupSelect.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








(0,vee_validate__WEBPACK_IMPORTED_MODULE_2__.extend)("required", {
  message: "{_field_} is required"
});
(0,vee_validate__WEBPACK_IMPORTED_MODULE_2__.extend)("confirmed", vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_3__.confirmed);
(0,vee_validate__WEBPACK_IMPORTED_MODULE_2__.extend)("required", vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_3__.required);
(0,vee_validate__WEBPACK_IMPORTED_MODULE_2__.extend)("min", _objectSpread(_objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_3__.min), {}, {
  message: 'the password must be 8 character or more'
}));
(0,vee_validate__WEBPACK_IMPORTED_MODULE_2__.extend)("regex", _objectSpread(_objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_3__.regex), {}, {
  message: 'Password requires 1 of each of the following: uppercase letter, lowercase letter, number.'
}));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    PrimeUploader: _components__WEBPACK_IMPORTED_MODULE_0__.PrimeUploader,
    FgSelect: _components_Inputs_formGroupSelect__WEBPACK_IMPORTED_MODULE_1__["default"],
    LSwitch: _components__WEBPACK_IMPORTED_MODULE_0__.Switch,
    FormGroupSelect: _components__WEBPACK_IMPORTED_MODULE_0__.FormGroupSelect,
    ImageUploader: _components__WEBPACK_IMPORTED_MODULE_0__.ImageUploader
  },
  data: function data() {
    return {
      editMode: false,
      loader: {},
      id: undefined,
      passVal: {
        required: true,
        min: 8,
        regex: '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).*$'
      },
      formTitle: "",
      entityNotFoundError: false,
      submitting: false,
      formData: {
        first_name: "",
        last_name: "",
        email: "",
        profile_image: "",
        is_active: true,
        role_id: null,
        password: ""
      },
      roles: []
    };
  },
  mounted: function mounted() {
    var _this = this;

    this.loader = this.$loading.show({
      container: this.$refs.userForm
    });
    this.id = this.$route.params['id'];

    if (this.id !== undefined) {
      this.editMode = true;
      this.formTitle = "Edit User";
    } else {
      this.editMode = false;
      this.formTitle = "Add User";
    }

    this.axios.post("users/builder").then(function (response) {
      _this.roles = response.data.roles;

      if (_this.editMode) {
        _this.getUser();
      } else {
        _this.loader.hide();
      }
    })["catch"](function (error) {
      console.error(error);
    });
  },
  methods: {
    getUser: function getUser() {
      var _this2 = this;

      this.axios.get("users/get/" + this.id).then(function (response) {
        _this2.formData = response.data;

        _this2.loader.hide();
      })["catch"](function (error) {
        if (error.response.status === 404) {
          _this2.entityNotFoundError = true;

          _this2.$notify({
            message: "User Not Found",
            timeout: 2000,
            type: 'danger'
          });

          _this2.loader.hide();
        } else {
          console.error(error);
        }
      });
    },
    submit: function submit() {
      var _this3 = this;

      var request;
      var successMessage;
      this.submitting = true;

      if (this.editMode === true) {
        var updateData = _objectSpread({}, this.formData);

        this.$delete(updateData, 'password');
        request = this.axios.put("users/update/" + this.id, updateData);
        successMessage = "User Updated Successfully";
      } else {
        request = this.axios.post("users/create", this.formData);
        successMessage = "User Added Successfully";
      }

      request.then(function (response) {
        _this3.$notify({
          message: successMessage,
          timeout: 1000,
          type: 'success'
        });

        _this3.$router.push("/users/list");
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this3.$refs.formValidator.setErrors(error.response.data.errors);
        } else {
          console.log(error.response);
        }
      })["finally"](function () {
        _this3.submitting = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserList.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserList.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var element_ui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! element-ui */ "./node_modules/element-ui/lib/element-ui.common.js");
/* harmony import */ var element_ui__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(element_ui__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components */ "./resources/js/vue/components/index.js");
/* harmony import */ var _components_GeneralDataTable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/GeneralDataTable */ "./resources/js/vue/components/GeneralDataTable.vue");
var _components;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: (_components = {
    NLPagination: _components__WEBPACK_IMPORTED_MODULE_1__.NewPagination,
    DeleteModal: _components__WEBPACK_IMPORTED_MODULE_1__.DeleteModal
  }, _defineProperty(_components, element_ui__WEBPACK_IMPORTED_MODULE_0__.TableColumn.name, element_ui__WEBPACK_IMPORTED_MODULE_0__.TableColumn), _defineProperty(_components, "LSwitch", _components__WEBPACK_IMPORTED_MODULE_1__.Switch), _defineProperty(_components, "GeneralDataTable", _components_GeneralDataTable__WEBPACK_IMPORTED_MODULE_2__["default"]), _components),
  data: function data() {
    return {
      tableColumns: [],
      fixedColumn: null,
      deleteModalVisibility: false,
      toDeleteId: undefined,
      loader: {}
    };
  },
  created: function created() {
    this.responsiveViewPort();
  },
  methods: {
    handleActiveStatus: function handleActiveStatus(row) {
      var _this = this;

      var data = {
        'is_active': row.is_active
      };
      this.axios.post("users/change-active-status/" + row.id, data).then(function (response) {
        _this.$notify({
          message: "User updated successfully",
          timeout: 1000,
          type: 'success'
        });
      })["catch"](function (error) {
        console.log(error);
      });
    },
    openDeleteModal: function openDeleteModal(id) {
      this.deleteModalVisibility = true;
      this.toDeleteId = id;
    },
    closeDeleteModal: function closeDeleteModal() {
      this.deleteModalVisibility = false;
    },
    confirmDeleteModal: function confirmDeleteModal() {
      var _this2 = this;

      var data = {
        'id': this.toDeleteId
      };
      this.axios["delete"]("users/delete", {
        headers: {},
        data: data
      }).then(function (response) {
        _this2.$notify({
          message: "User deleted successfully",
          timeout: 1000,
          type: 'success'
        });

        _this2.$refs.table.getData("updateData");
      })["catch"](function (error) {
        _this2.$notify({
          message: error.response.data.message,
          timeout: 2000,
          type: 'danger'
        });
      });
      this.closeDeleteModal();
      this.toDeleteId = undefined;
    },
    responsiveViewPort: function responsiveViewPort() {
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        this.fixedColumn = null;
        this.tableColumns = [{
          label: 'Full Name',
          value: 'full_name',
          minWidth: '200',
          align: 'center'
        }, {
          label: 'Role',
          value: 'role.name',
          minWidth: '200',
          align: 'center'
        }];
      } else {
        this.fixedColumn = 'right';
        this.tableColumns = [{
          label: 'Full Name',
          value: 'full_name',
          minWidth: '200',
          align: 'center'
        }, {
          label: 'Email',
          value: 'email',
          minWidth: '200',
          align: 'center'
        }, {
          label: 'Role',
          value: 'role.name',
          minWidth: '200',
          align: 'center'
        }];
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=template&id=53f3eaef&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=template&id=53f3eaef& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-xs-8 offset-xs-2 col-md-8 offset-md-2 col-sm-12"
  }, [_c("ValidationObserver", {
    ref: "formValidator",
    scopedSlots: _vm._u([{
      key: "default",
      fn: function fn(_ref) {
        var handleSubmit = _ref.handleSubmit;
        return [_c("card", [_c("div", {
          attrs: {
            slot: "header"
          },
          slot: "header"
        }, [_c("h4", {
          staticClass: "card-title"
        }, [_vm._v("\n            " + _vm._s(_vm.formTitle) + "\n          ")])]), _vm._v(" "), _c("div", {
          staticClass: "card-body"
        }, [_c("ValidationProvider", {
          attrs: {
            vid: "password",
            rules: _vm.passVal,
            name: "The Password"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref2) {
              var passed = _ref2.passed,
                  failed = _ref2.failed,
                  errors = _ref2.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "password",
                  error: failed ? errors[0] : null,
                  hasSuccess: passed,
                  label: "Password"
                },
                model: {
                  value: _vm.formData.password,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "password", $$v);
                  },
                  expression: "formData.password"
                }
              })];
            }
          }], null, true)
        }), _vm._v(" "), _c("ValidationProvider", {
          attrs: {
            rules: "required|confirmed:password",
            name: "The Confirmed Password"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref3) {
              var passed = _ref3.passed,
                  failed = _ref3.failed,
                  errors = _ref3.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "password",
                  error: failed ? errors[0] : null,
                  hasSuccess: passed,
                  label: "Confirm Password",
                  name: "confirm"
                },
                model: {
                  value: _vm.formData.confirmed_password,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "confirmed_password", $$v);
                  },
                  expression: "formData.confirmed_password"
                }
              })];
            }
          }], null, true)
        })], 1), _vm._v(" "), _c("div", {
          staticClass: "card-footer text-right"
        }, [_c("l-button", {
          attrs: {
            disabled: _vm.submitting,
            nativeType: "submit",
            type: "info",
            wide: ""
          },
          on: {
            click: function click($event) {
              $event.preventDefault();
              return handleSubmit(_vm.submit);
            }
          }
        }, [_vm._v("Submit\n          ")]), _vm._v(" \n          "), _c("l-button", {
          attrs: {
            type: "danger",
            wide: ""
          },
          on: {
            click: function click($event) {
              return _vm.$router.push("/users/list");
            }
          }
        }, [_vm._v("Cancel\n          ")])], 1)])];
      }
    }])
  })], 1)]);
};

var staticRenderFns = [];
render._withStripped = true;


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserForm.vue?vue&type=template&id=1feb5388&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserForm.vue?vue&type=template&id=1feb5388& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    ref: "userForm",
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-xs-12 col-md-12 col-sm-12"
  }, [_c("ValidationObserver", {
    ref: "formValidator",
    scopedSlots: _vm._u([{
      key: "default",
      fn: function fn(_ref) {
        var handleSubmit = _ref.handleSubmit;
        return [_c("card", [_c("div", {
          attrs: {
            slot: "header"
          },
          slot: "header"
        }, [_c("h4", {
          staticClass: "card-title"
        }, [_vm._v("\n            " + _vm._s(_vm.formTitle) + "\n          ")])]), _vm._v(" "), _c("div", {
          staticClass: "card-body"
        }, [_c("div", {
          staticClass: "row"
        }, [_c("div", {
          staticClass: "col-xs-6 col-md-6 col-sm-12"
        }, [_c("ValidationProvider", {
          attrs: {
            vid: "first_name",
            rules: "required",
            name: "First Name"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref2) {
              var passed = _ref2.passed,
                  failed = _ref2.failed,
                  errors = _ref2.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "text",
                  error: failed ? errors[0] : null,
                  label: "First Name",
                  name: "first_name",
                  fou: ""
                },
                model: {
                  value: _vm.formData.first_name,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "first_name", $$v);
                  },
                  expression: "formData.first_name"
                }
              })];
            }
          }], null, true)
        }), _vm._v(" "), _c("ValidationProvider", {
          attrs: {
            vid: "last_name",
            rules: "required",
            name: "Last Name"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref3) {
              var passed = _ref3.passed,
                  failed = _ref3.failed,
                  errors = _ref3.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "text",
                  error: failed ? errors[0] : null,
                  label: "Last Name",
                  name: "last_name",
                  fou: ""
                },
                model: {
                  value: _vm.formData.last_name,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "last_name", $$v);
                  },
                  expression: "formData.last_name"
                }
              })];
            }
          }], null, true)
        }), _vm._v(" "), _c("ValidationProvider", {
          attrs: {
            vid: "email",
            rules: "required|email",
            name: "Email"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref4) {
              var passed = _ref4.passed,
                  failed = _ref4.failed,
                  errors = _ref4.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "email",
                  error: failed ? errors[0] : null,
                  label: "Email",
                  name: "email",
                  fou: ""
                },
                model: {
                  value: _vm.formData.email,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "email", $$v);
                  },
                  expression: "formData.email"
                }
              })];
            }
          }], null, true)
        }), _vm._v(" "), _c("ValidationProvider", {
          attrs: {
            vid: "role_id",
            rules: "required",
            name: "The Role"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref5) {
              var passed = _ref5.passed,
                  failed = _ref5.failed,
                  errors = _ref5.errors;
              return [_c("fg-select", {
                attrs: {
                  name: "role_id",
                  size: "large",
                  filterable: "",
                  placeholder: "Select Role",
                  "input-classes": "select-default",
                  label: "Roles",
                  error: failed ? errors[0] : null,
                  list: _vm.roles,
                  listItemLabel: "name",
                  listItemValue: "id"
                },
                model: {
                  value: _vm.formData.role_id,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "role_id", $$v);
                  },
                  expression: "formData.role_id"
                }
              })];
            }
          }], null, true)
        }), _vm._v(" "), !_vm.editMode ? _c("ValidationProvider", {
          attrs: {
            vid: "password",
            rules: _vm.passVal,
            name: "The Password"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function fn(_ref6) {
              var passed = _ref6.passed,
                  failed = _ref6.failed,
                  errors = _ref6.errors;
              return [_c("fg-input", {
                attrs: {
                  type: "password",
                  error: failed ? errors[0] : null,
                  hasSuccess: passed,
                  label: "Password"
                },
                model: {
                  value: _vm.formData.password,
                  callback: function callback($$v) {
                    _vm.$set(_vm.formData, "password", $$v);
                  },
                  expression: "formData.password"
                }
              })];
            }
          }], null, true)
        }) : _vm._e(), _vm._v(" "), _c("div", {
          staticClass: "form-group"
        }, [_c("label", [_vm._v("Is Active")]), _vm._v(" \n                "), _c("l-switch", {
          model: {
            value: _vm.formData.is_active,
            callback: function callback($$v) {
              _vm.$set(_vm.formData, "is_active", $$v);
            },
            expression: "formData.is_active"
          }
        }, [_c("i", {
          staticClass: "fa fa-check",
          attrs: {
            slot: "on"
          },
          slot: "on"
        }), _vm._v(" "), _c("i", {
          staticClass: "fa fa-times",
          attrs: {
            slot: "off"
          },
          slot: "off"
        })])], 1)], 1), _vm._v(" "), _c("div", {
          staticClass: "col-xs-6 col-md-6 col-sm-12"
        }, [_c("div", {
          staticClass: "form-group"
        }, [_c("label", [_vm._v("Profile Image:")]), _vm._v(" "), _c("prime-uploader", {
          attrs: {
            "preview-width": "200px",
            "preview-height": "200px",
            elementNum: 1
          },
          model: {
            value: _vm.formData.profile_image,
            callback: function callback($$v) {
              _vm.$set(_vm.formData, "profile_image", $$v);
            },
            expression: "formData.profile_image"
          }
        })], 1)])])]), _vm._v(" "), _c("div", {
          staticClass: "card-footer text-right"
        }, [_c("l-button", {
          attrs: {
            disabled: _vm.entityNotFoundError || _vm.submitting,
            nativeType: "submit",
            type: "info",
            wide: ""
          },
          on: {
            click: function click($event) {
              $event.preventDefault();
              return handleSubmit(_vm.submit);
            }
          }
        }, [_vm._v("Submit\n          ")]), _vm._v(" "), _c("l-button", {
          attrs: {
            type: "danger",
            wide: ""
          },
          on: {
            click: function click($event) {
              return _vm.$router.push("/users/list");
            }
          }
        }, [_vm._v("Cancel\n          ")])], 1)])];
      }
    }])
  })], 1)]);
};

var staticRenderFns = [];
render._withStripped = true;


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserList.vue?vue&type=template&id=53e47a62&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserList.vue?vue&type=template&id=53e47a62& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    ref: "userList",
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-12"
  }, [_c("card", [_c("div", {
    staticClass: "col-12 d-flex justify-content-center justify-content-sm-between flex-wrap",
    attrs: {
      slot: "header"
    },
    slot: "header"
  }, [_c("h4", {
    staticClass: "card-title"
  }, [_vm._v("Users List")]), _vm._v(" "), _vm.$store.getters["auth/haveOneOfPermissions"](["users/create"]) ? _c("router-link", {
    staticClass: "btn btn-info btn-wd",
    attrs: {
      to: "/users/create"
    }
  }, [_vm._v("\n          Add New\n          "), _c("span", {
    staticClass: "btn-label"
  }, [_c("i", {
    staticClass: "fa fa-plus"
  })])]) : _vm._e()], 1), _vm._v(" "), _c("div", [_c("general-data-table", {
    ref: "table",
    attrs: {
      "api-url": "users/index"
    }
  }, [_vm._l(_vm.tableColumns, function (column) {
    return _c("el-table-column", {
      key: column.label,
      attrs: {
        "min-width": column.minWidth,
        align: column.align,
        sortable: column.sortable,
        prop: column.value,
        label: column.label
      }
    });
  }), _vm._v(" "), _c("el-table-column", {
    attrs: {
      "min-width": 120,
      fixed: _vm.fixedColumn,
      align: "center",
      label: "Active"
    },
    scopedSlots: _vm._u([{
      key: "default",
      fn: function fn(props) {
        return [_vm.$store.getters["auth/haveOneOfPermissions"](["users/change-active-status"]) ? _c("l-switch", {
          on: {
            input: function input($event) {
              return _vm.handleActiveStatus(props.row);
            }
          },
          model: {
            value: props.row.is_active,
            callback: function callback($$v) {
              _vm.$set(props.row, "is_active", $$v);
            },
            expression: "props.row.is_active"
          }
        }, [_c("i", {
          staticClass: "fa fa-check",
          attrs: {
            slot: "on"
          },
          slot: "on"
        }), _vm._v(" "), _c("i", {
          staticClass: "fa fa-times",
          attrs: {
            slot: "off"
          },
          slot: "off"
        })]) : _vm._e()];
      }
    }])
  }), _vm._v(" "), _c("el-table-column", {
    attrs: {
      "min-width": 120,
      fixed: _vm.fixedColumn,
      align: "center",
      label: "Actions"
    },
    scopedSlots: _vm._u([{
      key: "default",
      fn: function fn(scope) {
        return [_vm.$store.getters["auth/checkAccess"]("users/change-password") ? _c("router-link", {
          directives: [{
            name: "tooltip",
            rawName: "v-tooltip.top-center",
            value: "Change Password",
            expression: "'Change Password'",
            modifiers: {
              "top-center": true
            }
          }],
          staticClass: "btn-info btn-simple btn-link",
          attrs: {
            to: "/users/changePassword/" + scope.row.id
          }
        }, [_c("i", {
          staticClass: "fa fa-key"
        })]) : _vm._e(), _vm._v(" "), _vm.$store.getters["auth/haveOneOfPermissions"](["users/update"]) ? _c("router-link", {
          directives: [{
            name: "tooltip",
            rawName: "v-tooltip.top-center",
            value: "Edit",
            expression: "'Edit'",
            modifiers: {
              "top-center": true
            }
          }],
          staticClass: "btn-warning btn-simple btn-link",
          attrs: {
            to: "/users/edit/" + scope.row.id
          }
        }, [_c("i", {
          staticClass: "fa fa-edit"
        })]) : _vm._e(), _vm._v(" "), _vm.$store.getters["auth/haveOneOfPermissions"](["users/delete"]) ? _c("a", {
          staticClass: "btn-danger btn-simple btn-link",
          on: {
            click: function click($event) {
              return _vm.openDeleteModal(scope.row.id);
            }
          }
        }, [_c("i", {
          staticClass: "fa fa-times"
        })]) : _vm._e()];
      }
    }])
  })], 2)], 1)]), _vm._v(" "), _c("delete-modal", {
    attrs: {
      visible: _vm.deleteModalVisibility,
      message: "Are you sure you want to delete this user?"
    },
    on: {
      close: function close($event) {
        return _vm.closeDeleteModal();
      },
      confirm: function confirm($event) {
        return _vm.confirmDeleteModal();
      }
    }
  })], 1)]);
};

var staticRenderFns = [];
render._withStripped = true;


/***/ }),

/***/ "./resources/js/vue/pages/users/ChangeUserPassword.vue":
/*!*************************************************************!*\
  !*** ./resources/js/vue/pages/users/ChangeUserPassword.vue ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ChangeUserPassword_vue_vue_type_template_id_53f3eaef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ChangeUserPassword.vue?vue&type=template&id=53f3eaef& */ "./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=template&id=53f3eaef&");
/* harmony import */ var _ChangeUserPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChangeUserPassword.vue?vue&type=script&lang=js& */ "./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ChangeUserPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ChangeUserPassword_vue_vue_type_template_id_53f3eaef___WEBPACK_IMPORTED_MODULE_0__.render,
  _ChangeUserPassword_vue_vue_type_template_id_53f3eaef___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/pages/users/ChangeUserPassword.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/pages/users/UserForm.vue":
/*!***************************************************!*\
  !*** ./resources/js/vue/pages/users/UserForm.vue ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _UserForm_vue_vue_type_template_id_1feb5388___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserForm.vue?vue&type=template&id=1feb5388& */ "./resources/js/vue/pages/users/UserForm.vue?vue&type=template&id=1feb5388&");
/* harmony import */ var _UserForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserForm.vue?vue&type=script&lang=js& */ "./resources/js/vue/pages/users/UserForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserForm_vue_vue_type_template_id_1feb5388___WEBPACK_IMPORTED_MODULE_0__.render,
  _UserForm_vue_vue_type_template_id_1feb5388___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/pages/users/UserForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/pages/users/UserList.vue":
/*!***************************************************!*\
  !*** ./resources/js/vue/pages/users/UserList.vue ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _UserList_vue_vue_type_template_id_53e47a62___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserList.vue?vue&type=template&id=53e47a62& */ "./resources/js/vue/pages/users/UserList.vue?vue&type=template&id=53e47a62&");
/* harmony import */ var _UserList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserList.vue?vue&type=script&lang=js& */ "./resources/js/vue/pages/users/UserList.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserList_vue_vue_type_template_id_53e47a62___WEBPACK_IMPORTED_MODULE_0__.render,
  _UserList_vue_vue_type_template_id_53e47a62___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/pages/users/UserList.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangeUserPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ChangeUserPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangeUserPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/pages/users/UserForm.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/vue/pages/users/UserForm.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./UserForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/pages/users/UserList.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/vue/pages/users/UserList.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./UserList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserList.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=template&id=53f3eaef&":
/*!********************************************************************************************!*\
  !*** ./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=template&id=53f3eaef& ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangeUserPassword_vue_vue_type_template_id_53f3eaef___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangeUserPassword_vue_vue_type_template_id_53f3eaef___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangeUserPassword_vue_vue_type_template_id_53f3eaef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ChangeUserPassword.vue?vue&type=template&id=53f3eaef& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/ChangeUserPassword.vue?vue&type=template&id=53f3eaef&");


/***/ }),

/***/ "./resources/js/vue/pages/users/UserForm.vue?vue&type=template&id=1feb5388&":
/*!**********************************************************************************!*\
  !*** ./resources/js/vue/pages/users/UserForm.vue?vue&type=template&id=1feb5388& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserForm_vue_vue_type_template_id_1feb5388___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserForm_vue_vue_type_template_id_1feb5388___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserForm_vue_vue_type_template_id_1feb5388___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./UserForm.vue?vue&type=template&id=1feb5388& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserForm.vue?vue&type=template&id=1feb5388&");


/***/ }),

/***/ "./resources/js/vue/pages/users/UserList.vue?vue&type=template&id=53e47a62&":
/*!**********************************************************************************!*\
  !*** ./resources/js/vue/pages/users/UserList.vue?vue&type=template&id=53e47a62& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserList_vue_vue_type_template_id_53e47a62___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserList_vue_vue_type_template_id_53e47a62___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserList_vue_vue_type_template_id_53e47a62___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./UserList.vue?vue&type=template&id=53e47a62& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/pages/users/UserList.vue?vue&type=template&id=53e47a62&");


/***/ })

}]);